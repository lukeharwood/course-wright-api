const mongoose = require("mongoose");

// [docs](https://mongoosejs.com/docs/schematypes.html]
const courseSchema = new mongoose.Schema({
    creator: { type: mongoose.ObjectId, required: true },
    dateCreated: { type: Date, default: Date.now },
    name: { type: String, required: true },
    description: String,
    code: String,
    subject: String,
    photoId: String,
    color: String,
    tags: [{ type: String, required: true }],
    visibility: { type: String, required: true },
    shareOption: { type: String, required: true },
    access: [
        {
            user: mongoose.ObjectId,
            role: String,
        },
    ],
    sections: [mongoose.ObjectId],
    link: { type: Boolean, default: false },
    homePage: mongoose.ObjectId,
    directories: [
        {
            files: [
                {
                    name: String,
                    uri: mongoose.ObjectId
                }
            ],
            pages: [
                {
                    title: String,
                    uri: mongoose.ObjectId
                }
            ]
        }
    ]
});

const Course = mongoose.model("Course", courseSchema);

module.exports = { Course };
